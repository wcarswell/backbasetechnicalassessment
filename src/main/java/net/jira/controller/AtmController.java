package net.jira.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import net.jira.bean.Atm;
import net.jira.service.AtmService;

@RestController
public class AtmController {
	@Autowired
	private AtmService atmService;
	
	@RequestMapping(value = "/atms/{atmType}", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Atm> getAtms(@PathVariable String atmType) {
		return atmService.filterByType(atmType);
	}
	
	@RequestMapping(value = "/atms/{atmType}/{cityName}", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Atm> getAtms(@PathVariable String atmType, @PathVariable String cityName) {
		return atmService.filterByTypeAndCity(atmType, cityName);
	}
	
	@RequestMapping(value = "/atms/", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Atm> getAllAtms() {
		return atmService.getAll();
	}
}