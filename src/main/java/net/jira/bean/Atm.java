package net.jira.bean;

public class Atm {
	private String type;
	private int distance;
	private Address address;
	
	public Atm(String type, int distance) {
		this.type = type;
		this.distance = distance;
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Atm [type=" + type + ", distance=" + distance + ", address=" + address + "]";
	}
}
