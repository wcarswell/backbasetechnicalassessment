package net.jira.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.jira.bean.Atm;

@Service
public class AtmService {
	private static List<Atm> atmList;
	
	public static List<Atm> getAtmList() {
		return atmList;
	}

	public static void setAtmList(List<Atm> atmList) {
		AtmService.atmList = atmList;
	}
	
	@Override
	public String toString() {
		return "AtmService [toString()=" + super.toString() + "]";
	}
	
	public List<Atm> getAll() {
		//  Complete exchange and intialize response variable
		ResponseEntity<String> response = fetchAtmList();
		
		// This particular example is malformed, and below is quick hack
		String responseBody = response.getBody();
		if(!isJSONValid(response.getBody())) {
			// Remove the first 5 odd characters
			responseBody = responseBody.substring(5, responseBody.length());
		} 
		
		// Convert Json to Atm Object
		Gson gson = new GsonBuilder().create(); 
		Atm[] superSet = gson.fromJson(responseBody, Atm[].class );
		
		// Convert to ArrayList and only show by type
		atmList = new ArrayList<Atm>(Arrays.asList(superSet));
		return atmList;
	}
	

	public List<Atm> filterByType(String atmType) {
		//  Complete exchange and intialize response variable
		ResponseEntity<String> response = fetchAtmList();
		
		// This particular example is malformed, and below is quick hack
		String responseBody = response.getBody();
		if(!isJSONValid(response.getBody())) {
			// Remove the first 5 odd characters
			responseBody = responseBody.substring(5, responseBody.length());
		} 
		
		// Convert Json to Atm Object
		Gson gson = new GsonBuilder().create(); 
		Atm[] superSet = gson.fromJson(responseBody, Atm[].class );
		
		// Convert to ArrayList and only show by type
		atmList = new ArrayList<Atm>(Arrays.asList(superSet));
		for (Iterator<Atm> iterator = atmList.iterator(); iterator.hasNext();) {
		    Atm obj = iterator.next();
		    if(!obj.getType().equalsIgnoreCase(atmType)) {
				iterator.remove();
			}
		}
		return atmList;
	}
	
	public List<Atm> filterByTypeAndCity(String atmType, String cityName) {
		//  Complete exchange and intialize response variable
		ResponseEntity<String> response = fetchAtmList();
		
		// This particular example is malformed, and below is quick hack
		String responseBody = response.getBody();
		if(!isJSONValid(response.getBody())) {
			// Remove the first 5 odd characters
			responseBody = responseBody.substring(5, responseBody.length());
		} 
		
		// Convert Json to Atm Object
		Gson gson = new GsonBuilder().create(); 
		Atm[] superSet = gson.fromJson(responseBody, Atm[].class );
		
		// Convert to ArrayList and only show by type
		atmList = new ArrayList<Atm>(Arrays.asList(superSet));
		for (Iterator<Atm> iterator = atmList.iterator(); iterator.hasNext();) {
		    Atm obj = iterator.next();
		    
		    if(!obj.getType().toLowerCase().equals(atmType) ||
		    		!obj.getAddress().getCity().equalsIgnoreCase(cityName)) {
				iterator.remove();
			}
		}
		return atmList;
	}
	
	public ResponseEntity<String> fetchAtmList() {
		//TODO: Add to generic properties file
		// External URl
		String url = "https://www.ing.nl/api/locator/atms/";
		
		// Use Rest Template to get external Data
		RestTemplate restTemplate = new RestTemplate();
		
		// 	Set Http Headers
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		
		return  restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	}
	
	
	public boolean isJSONValid(String test) {
		Gson gson = new Gson();
		try {
			gson.fromJson(test, Object.class);
			return true;
		} catch (com.google.gson.JsonSyntaxException ex) {
			return false;
		}
	}

	public void getSize() {
		// TODO Auto-generated method stub
		
	}
}
