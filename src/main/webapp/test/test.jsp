<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Jasmine References -->
<link rel="stylesheet" type="text/css"
	href="https://cdnjs.cloudflare.com/ajax/libs/jasmine/2.3.3/jasmine.min.css">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jasmine/2.3.3/jasmine.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jasmine/2.3.3/jasmine-html.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jasmine/2.3.3/boot.min.js"></script>

<!-- Angular and Angular Mock references -->
<script type="text/javascript"
	src="https://code.angularjs.org/1.4.0-rc.2/angular.min.js"></script>
<script type="text/javascript"
	src="https://code.angularjs.org/1.4.0-rc.2/angular-mocks.js"></script>
</head>
<body></body>
<script type="text/javascript">
angular.module('atmApp', []);

// Registers a controller to our module 'calculatorApp'.
angular.module('atmApp').controller('AtmController', function AtmController($scope, $http) {
	$http.get('/backbase-ta/atms').
     success(function(data) {
         $scope.atms = data;
     });
});

// Load the app
angular.element(document).ready(function() {
		angular.bootstrap(document, ['atmApp']);
});
    
describe("atmApp", function () {
	 
    beforeEach(module('atmApp'));
 
    describe("AtmController", function () {
 
        var scope, httpBackend, http, controller;
        beforeEach(inject(function ($rootScope, $controller, $httpBackend, $http) {
            scope = $rootScope.$new();
            httpBackend = $httpBackend;
            http = $http;
            controller = $controller;
            httpBackend.when("GET", "/backbase-ta/atms").respond([{}, {}, {}]);
        }));
       
        it('should GET Atm List', function () {
            httpBackend.expectGET('/backbase-ta/atms');
            controller('AtmController', {
                $scope: scope,
                $http: http
            });
            httpBackend.flush();
            expect(scope.atms.length).toBe(3);
        });
    });
});
  </script>
</html>