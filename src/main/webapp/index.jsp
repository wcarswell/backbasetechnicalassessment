<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>
<head>
<title>Demo of ING Atm</title>
<meta name="viewport" content="initial-scale=1.0, width=device-width" />
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>
<script type="text/javascript"
	src="https://code.angularjs.org/1.4.0-rc.2/angular.min.js"></script>

<link
	href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css"
	rel="stylesheet">

<script src="http://js.api.here.com/v3/3.0/mapsjs-core.js"
	type="text/javascript" charset="utf-8"></script>
<script src="http://js.api.here.com/v3/3.0/mapsjs-service.js"
	type="text/javascript" charset="utf-8"></script>
<script>
	angular.module('atmApp', []);

	// Registers a controller to our module 'calculatorApp'.
	angular.module('atmApp').controller('AtmController',
		function AtmController($scope, $http) {
			$scope.busyLoading = false;
			// Initial AtmType
			$scope.atmType = 'ing';
			$scope.cityName = '';
			
			$scope.redoAtmList = function(){
				$scope.busyLoading = true;
				$http.get('/backbase-ta/atms/' + $scope.atmType + "/" + $scope.cityName).success(function(data) {
					$scope.atms = data;
					$scope.busyLoading = false;
				});
				
			}
			
			$scope.redoAtmList();
	});

	// Load the app
	angular.element(document).ready(function() {
		angular.bootstrap(document, [ 'atmApp' ]);
	});
</script>

</head>
<body ng-controller="AtmController">

	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="jumbotron">
		<div class="container">
			<h1>Hey Backbase</h1>
			<p>Here is the list of ING ATMs</p>
			<p>
				<a class="btn btn-primary btn-lg"
					href="<c:url value="j_spring_security_logout" />" role="button">Logout</a>
			</p>
			<!-- Filter Buttons -->
			<p>
				<a class="btn btn-primary btn-lg clearfix" role="button" ng-click="atmType='';cityName='';redoAtmList();">Display All</a>
				<a class="btn btn-primary btn-lg clearfix" role="button" ng-click="atmType='ing';cityName='';redoAtmList();">Display ING ATMs</a>
				<a class="btn btn-primary btn-lg clearfix" role="button" ng-click="atmType='ing';cityName='ALKMAAR';redoAtmList();">Display ALKMAAR ING ATMs</a>
				<a class="btn btn-primary btn-lg clearfix" role="button" ng-click="atmType='somethingwrong';cityName='';redoAtmList();">Display No Results</a>
			</p>
		</div>
	</div>

	<div id="map_wrapper">
		<div id="map_canvas" class="mapping"></div>
	</div>

	<div class="container">
		<!-- Example row of columns -->
		<div class="row">
			<div class="col-md-12">
				<div>
					<!--  Show Busy Loading Banner -->
					<div class="alert alert-info" ng-show="busyLoading" role="alert">Busy Loading....</div>
					
					<!--  Show Results Message -->
					<div class="alert alert-success" ng-show="atms.length>0 && !busyLoading" role="alert">There are {{atms.length}} result{{atms.length == 1 ? "" : "s"}} for {{atmType == "" ? "All" : atmType | uppercase}} ATMs</div>
					
					<!--  Show No Results Message -->
					<div class="alert alert-warning" ng-show="atms.length==0" role="alert">There are no Results for ATM Type <b>{{atmType | uppercase}}</b></div>
					
					<!-- Table of Results -->
					<table id="atm-list" class="table table-striped" ng-show="!busyLoading">
						<tr ng-show="atms.length>0">
							<th>City</th>
							<th>Street</th>
							<th>Housenumber</th>
							<th>PostalCode</th>
							<th>Latitude</th>
							<th>Longitude</th>
							<th>Distance</th>
							<th>Type</th>
						</tr>
						<tr
							ng-repeat="atm in atms | orderBy:['address.city','address.street']">
							<td>{{atm.address.city}}</td>
							<td>{{atm.address.street}}</td>
							<td>{{atm.address.housenumber}}</td>
							<td>{{atm.address.postalcode}}</td>
							<td><i class="fa fa-map-marker fa-fw"></i>
								{{atm.address.geoLocation.lat}}</td>
							<td><i class="fa fa-map-marker fa-fw"></i>
								{{atm.address.geoLocation.lng}}</td>
							<td>{{atm.distance}}</td>
							<td>{{atm.type}}</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- /container -->
</body>
</html>