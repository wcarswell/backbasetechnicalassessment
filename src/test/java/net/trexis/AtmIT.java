package net.trexis;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.junit.Test;

public class AtmIT {
	/**
     * Test if the api service is running and if the admin user is available.
     *
     * @throws IOException when caused by executeMethod
     */
	@Test
    public void basicValidation() throws IOException {
        // Request url assuming default set up
        String url = "http://localhost:8080/backbase-ta/atms/ing";

        // Set up the getMethod
        GetMethod get = new GetMethod(url);
        get.setFollowRedirects(false);
        get.setRequestHeader("Authorization", "Basic " + new String(Base64.encodeBase64(("admin:password").getBytes())));

        // Execute the request
        HttpClient adminClient = new HttpClient();
        int resCode = adminClient.executeMethod(get);
        
        // Check if the http status code is 200 (OK)
        assertEquals("GET (" + url + ")", 200, resCode);
    }
	
	@Test
	public void testExternalAtmUrl()  throws IOException {
		// Request url assuming default set up
		String url = "https://www.ing.nl/api/locator/atms/";

        // Set up the getMethod
        GetMethod get = new GetMethod(url);
        get.setFollowRedirects(false);
        
        // Execute the request
        HttpClient adminClient = new HttpClient();
        int resCode = adminClient.executeMethod(get);
        
        // Check if the http status code is 200 (OK)
        assertEquals("GET (" + url + ")", 200, resCode);
	}
	
}