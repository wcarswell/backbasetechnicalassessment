package net.trexis;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import net.jira.bean.Atm;
import net.jira.controller.AtmController;
import net.jira.service.AtmService;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class AtmControllerTest {

	private MockMvc mockMvc;

    @Mock
    private AtmService atmService;

    @InjectMocks
    private AtmController atmController;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(atmController)
                .build();
    }
    
    @Test
    public void canGetAtmControllerResult() throws Exception {
    	List<Atm> atmList = Arrays.asList(
                new Atm("ing", 300),
                new Atm("ing", 200)
        );

        when(atmService.filterByType("ing")).thenReturn(atmList);

        mockMvc.perform(get("/atms/ing"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print());

        verify(atmService, times(1)).filterByType("ing");
        verifyNoMoreInteractions(atmService);
    }
}