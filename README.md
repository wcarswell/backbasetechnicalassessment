# Backbase Technical Assessment - William Carswell[31 October 2016]

### Introduction
This repository includes a solution for the Backbase Recruiting process and uses the XML Rest DSL as implementation

### File Structure
* +-- pom.xml // Maven Pom file to load dependencies
* +-- Readme.docx // Instruction on how I implemented the project
* +-- backbase-ta - Spring Tool Suite(3.8.1.RELEASE)-dist.zip // Zip of whole project
* +-- backbase-ta.war // Java War file that can be executed in a vanilla Tomcat 7 instance
      // Executed at http://localhost:8080/backbase-ta/
* +-- src
* |   +-- assembly // includes instruction for zipping repo
* |   +-- main // our main web app
* |   +-- test // unit tests

### Build

You will need to (default)
* compile this example first:

	mvn clean install

### Package Zip File
	mvn assembly:single